var express = require("express"),
    app = express();
var port = process.env.PORT || 3000;

app.get("/", function(req, res) {
    return res.json({message: "Hello from expresjs !"});
});

app.listen(port, function() {
    console.log("App listening on port: " + port);
});

module.exports = app; // for testing
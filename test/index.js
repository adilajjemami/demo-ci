//During the test the env variable is set to test
process.env.NODE_ENV = "test";

let chai = require("chai");
let chaiHttp = require("chai-http");
let app = require("../src/index");
let should = chai.should();
chai.use(chaiHttp);

describe("Application", function() {
    it("it should get base url", done => {
      chai
        .request(app)
        .get("/")
        .end((err, res) => {
            should.not.exist(err);
            res.status.should.be.eql(200);
          done();
          process.exit(0);
        });
    });
});

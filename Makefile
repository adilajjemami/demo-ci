# Build
build:
	docker-compose build
build-dev:
	docker-compose -f docker-compose.dev.yml build

# Docker compose UP
up:
	docker-compose up
up-dev:
	docker-compose -f docker-compose.dev.yml up

# Remove image
remove:
	docker container rm democi

# SSH to specefic container
ssh:
	docker exec -it  $(id) sh

# NPM scripts
install:
	npm install
test:
	npm run test
start:
	npm run start
dev:
	npm run dev
lint:
	npm run lint
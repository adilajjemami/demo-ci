FROM node:9.5

ENV NODE_ENV developpement
ENV PORT 3000
ARG port=$PORT

RUN mkdir -p /usr/src/app
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN npm install
EXPOSE $port
#CMD ["node_modules/.bin/nodemon", "src/index.js"]